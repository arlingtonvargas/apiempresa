﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ApiEmpresa.Controllers
{
    public class TokenController : Controller
    {
        private const string SECRET_KEY = "db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==";
        public static readonly SymmetricSecurityKey SINGNKEY = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(TokenController.SECRET_KEY));

        [HttpGet]
        [Route("api/Token/{user}/{pass}")]
        public  IActionResult Get(string User, string Pass)
        {
            if (User == Pass)
                return new ObjectResult(GenerarteToken(User));
            else
                return BadRequest();
        }

        public string GenerarteToken(string User)
        {
            var token = new JwtSecurityToken(
                claims: new Claim[]
                {
                    new Claim(ClaimTypes.Name, User)
                }
                ,notBefore: new DateTimeOffset(DateTime.Now).DateTime
                ,expires: new DateTimeOffset(DateTime.Now.AddMinutes(5)).DateTime
                ,signingCredentials: new SigningCredentials(SINGNKEY, SecurityAlgorithms.HmacSha256)
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
