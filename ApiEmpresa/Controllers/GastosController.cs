﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BL.Gastos;
using ET.Gastos;
using ApiEmpresa.Models;

namespace ApiEmpresa.Controllers
{
    [Route("api/gastos")]
    [ApiController]
    public class GastosController : ControllerBase
    {
        GastosBL clGastos = new GastosBL();
        // GET: api/Gastos
        [HttpGet]
        public IEnumerable<GastosET> Get()
        {
            return clGastos.GetGastos();
        }

        // GET: api/Gastos/5
        [HttpGet("{id}", Name = "Get")]
        public GastosET Get(int id)
        {
            return clGastos.GetGasto(id);
        }

        // POST: api/Gastos
        [HttpPost]
        public IActionResult Post([FromBody] List<GastosET> Gastos)
        {
            List<GastosET> GastosRes = clGastos.InsertGastos(Gastos);
            if (GastosRes != null)
                return Ok(new ObjectResponse()
                {
                    Entity = GastosRes,
                    Mensaje = "OK",
                    Estado = true
                });
            else
                return Ok(new ObjectResponse()
                {
                    Entity = null,
                    Mensaje = "Error",
                    Estado = false
                });
            
        }
    }
}
