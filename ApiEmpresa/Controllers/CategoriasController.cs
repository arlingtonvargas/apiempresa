﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BL.Category;
using ET.Category;
using ApiEmpresa.Models;

namespace ApiEmpresa.Controllers
{  

    [Route("api/categorias")]
    [ApiController]
    public class CategoriasController : ControllerBase
    {
        CategoriasBL clCategories = new CategoriasBL();

        // GET: api/Categorias
        [HttpGet]
        public IEnumerable<CategoriasET> Get()
        {
            return clCategories.GetCategories();
        }

        // GET: api/Categorias/5
        [HttpGet("{id}")]
        public CategoriasET Get(int id)
        {
            return clCategories.GetCategory(id);
        }

        // POST: api/Categorias
        [HttpPost]
        public IActionResult Post([FromBody] CategoriasET value)
        {
            CategoriasET CategoryRes = clCategories.InsertCategory(value);
            if (CategoryRes != null && CategoryRes.IdCat > 0)
                return Ok(new ObjectResponse()
                {
                    Entity = CategoryRes,
                    Mensaje = "OK",
                    Estado = true
                });
            else
                return Ok(new ObjectResponse()
                {
                    Entity = null,
                    Mensaje = "Error",
                    Estado = false
                });
        }

        // PUT: api/Categorias/5
        [HttpPut]
        public IActionResult Put([FromBody] CategoriasET value)
        {
            CategoriasET CategoryRes = clCategories.UpdateCategory(value);
            if (CategoryRes != null && CategoryRes.IdCat > 0)
                return Ok(new ObjectResponse()
                {
                    Entity = CategoryRes,
                    Mensaje = "OK",
                    Estado = true
                });
            else
                return Ok(new ObjectResponse()
                {
                    Entity = null,
                    Mensaje = "Error",
                    Estado = false
                });
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            bool Elimino = clCategories.DeleteCategory(id);
            if (Elimino)
                return Ok(new ObjectResponse()
                {
                    Entity = null,
                    Mensaje = "OK",
                    Estado = true
                });
            else
                return Ok(new ObjectResponse()
                {
                    Entity = null,
                    Mensaje = "Error",
                    Estado = false
                });
        }
    }
}
