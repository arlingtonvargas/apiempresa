﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BL.Products;
using ET.Productos;
using ApiEmpresa.Models;

namespace ApiEmpresa.Controllers
{
    [Route("api/productos")]
    [ApiController]
    public class ProductosController : ControllerBase
    {
        ProductsBL clProductos = new ProductsBL();
        // GET: api/Productos
        [HttpGet]
        public IEnumerable<ProductsET> Get()
        {
            return clProductos.GetProducts();
        }

        // GET: api/Productos/5
        [HttpGet("{id}")]
        public ProductsET Get(int id)
        {
            return clProductos.GetProduct(id);
        }

        // POST: api/Productos
        [HttpPost]
        public IActionResult Post([FromBody]ProductsET value)
        {
            ProductsET ProductRes = clProductos.InsertProduct(value);
            if(ProductRes != null && ProductRes.Sec>0)
                return Ok(new ObjectResponse()
                {
                    Entity = ProductRes,
                    Mensaje = "OK",
                    Estado = true
                });
            else
                return Ok(new ObjectResponse()
                {
                    Entity = null,
                    Mensaje = "Error",
                    Estado = false
                });
        }

        // PUT: api/Productos/5
        [HttpPut]
        public IActionResult Put([FromBody] ProductsET value)
        {
            ProductsET ProductRes = clProductos.UpdateProduct(value);
            if (ProductRes != null && ProductRes.Sec > 0)
                return Ok(new ObjectResponse()
                {
                    Entity = ProductRes,
                    Mensaje = "OK",
                    Estado = true
                });
            else
                return Ok(new ObjectResponse()
                {
                    Entity = null,
                    Mensaje = "Error",
                    Estado = false
                });
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            bool Elimino = clProductos.DeleteProduct(id);
            if (Elimino)
                return Ok(new ObjectResponse()
                {
                    Entity = null,
                    Mensaje = "OK",
                    Estado = true
                });
            else
                return Ok(new ObjectResponse()
                {
                    Entity = null,
                    Mensaje = "Error",
                    Estado = false
                });
        }
    }
}
