﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ET.Login;
using BL.Login;
using ApiEmpresa.Models;


namespace ApiEmpresa.Controllers
{
    [Route("api/login")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        TokenController clToken = new TokenController();
        // GET: api/Login
        [HttpPost]
        public IActionResult Post([FromBody]LoginET value)
        {
            LoginBL bl = new LoginBL();
            var usuario = bl.Login(value.NomUsu, value.Pass);
            if (usuario != null)
            {
                return Ok(new LoginResponse()
                {
                    Usuario = usuario,
                    AccessToken = clToken.GenerarteToken(value.NomUsu),
                    Mensaje = "Autenticado",
                    Estado = true
                });
            }
            else
            {
                return Ok(new LoginResponse()
                {
                    Usuario = null,
                    AccessToken = null,
                    Mensaje = "Usuario o contraseña inválido",
                    Estado = false
                });
            }
        }        
    }
}
