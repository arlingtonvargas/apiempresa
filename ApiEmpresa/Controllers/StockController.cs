﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ET.RegistroStock;
using BL.RegistroStock;
using ApiEmpresa.Models;

namespace ApiEmpresa.Controllers
{
    [Route("api/stock")]
    [ApiController]
    public class StockController : ControllerBase
    {
        RegistroStockBL clRegistroStock = new RegistroStockBL();
        // GET: api/Stock
        [HttpGet]
        public IEnumerable<RegistroStockET> Get()
        {
            return clRegistroStock.GetRegistrosStock();
        }

        // GET: api/Stock/5
        [HttpGet("{id}")]
        public RegistroStockET Get(int id)
        {
            return clRegistroStock.GetRegistroStock(id);
        }

        // POST: api/Stock
        [HttpPost]
        public IActionResult Post([FromBody] RegistroStockET value)
        {
            RegistroStockET RegistroStockRes = clRegistroStock.InsertRegistroStock(value);
            if (RegistroStockRes != null)
                return Ok(new ObjectResponse()
                {
                    Entity = RegistroStockRes,
                    Mensaje = "OK",
                    Estado = true
                });
            else
                return Ok(new ObjectResponse()
                {
                    Entity = null,
                    Mensaje = "Error",
                    Estado = false
                });
        }

    }
}
