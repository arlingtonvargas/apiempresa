﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DA.Sales;
using ET.Sales;
using ApiEmpresa.Models;

namespace ApiEmpresa.Controllers
{
    [Route("api/ventas")]
    [ApiController]
    public class VentasController : ControllerBase
    {

        SalesDA clSales = new SalesDA();

        // GET: api/Ventas
        [HttpGet]
        public IEnumerable<VentasET> Get()
        {
            return clSales.GetSales();
        }

        // GET: api/Ventas/5
        [HttpGet("{id}")]
        public VentasET Get(int id)
        {
            return clSales.GetSale(id);
        }

        // POST: api/Ventas
        [HttpPost]
        public IActionResult Post([FromBody] VentasET value)
        {
            VentasET VentasRes = clSales.InsertSale(value);
            if (VentasRes != null)
                return Ok(new ObjectResponse()
                {
                    Entity = VentasRes,
                    Mensaje = "OK",
                    Estado = true
                });
            else
                return Ok(new ObjectResponse()
                {
                    Entity = null,
                    Mensaje = "Error",
                    Estado = false
                });
        }
       
    }
}
