﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEmpresa.Models
{
    public class LoginResponse: GeneralResponse
    {
        public ET.Login.LoginET Usuario { get; set; }
        /// <summary>
        /// Se debe mandar en el Header variable Authorization
        /// </summary>
        public string AccessToken { get; set; }
    }
}
