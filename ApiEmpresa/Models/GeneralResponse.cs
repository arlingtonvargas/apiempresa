﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEmpresa.Models
{
    public class GeneralResponse
    {
        public string Mensaje { get; set; }
        public bool Estado { get; set; }
    }
}
