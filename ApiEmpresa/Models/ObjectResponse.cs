﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEmpresa.Models
{
    public class ObjectResponse: GeneralResponse
    {
        public object Entity { get; set; }
    }
}
