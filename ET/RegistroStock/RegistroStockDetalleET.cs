﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ET.RegistroStock
{
    public class RegistroStockDetalleET
    {
        public int IdRegistroStockDetalle { get; set; }
        public int IdRegistroStock { get; set; }
        public int IdProd { get; set; }
        public int Cantidad { get; set; }
        public int CantActual { get; set; }
        public int CantFinal { get; set; }
    }
}
