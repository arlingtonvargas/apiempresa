﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ET.RegistroStock
{
    public class RegistroStockET
    {
        public int Sec { get; set; }
        public DateTime FechaReg { get; set; }
        public DateTime FechaEnvio { get; set; }        
        public List<RegistroStockDetalleET> RegStockDetalle { get; set; }
    }
}
