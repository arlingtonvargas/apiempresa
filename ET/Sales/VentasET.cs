﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ET.Sales
{
    public class VentasET
    {
        public int Sec { get; set; }
        public DateTime FechaVenta { get; set; }
        public DateTime FechaRegistro { get; set; }
        public List<VentaDetalleET> VentasDetalle { get; set; }
    }
}
