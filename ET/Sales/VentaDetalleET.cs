﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ET.Sales
{
    public class VentaDetalleET
    {
        public int IdVentaDetalle { get; set; }
        public int IdVenta { get; set; }
        public int IdProd { get; set; }
        public int Cantidad { get; set; }
        public decimal ValUni { get; set; }
        public decimal ValTo { get; set; }
    }
}
