﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ET.Productos
{
    public class ProductsET
    {
        public int Sec { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioCosto { get; set; }
        public decimal PrecioVenta { get; set; }
        public DateTime FechaReg { get; set; }
        public DateTime FechaMod { get; set; }
        public int IdCategoria { get; set; }
    }

    public class ListaProductos: List<ProductsET> { }
}
