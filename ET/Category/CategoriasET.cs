﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ET.Category
{
    public class CategoriasET
    {
        public int IdCat { get; set; }
        public string NomCat { get; set; }
    }
    public class ListaCategorias : List<CategoriasET> { }
}
