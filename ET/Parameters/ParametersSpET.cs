﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ET.Parameters
{
    public class ParametersSpET
    {
        public string NombreParametro { get; set; }
        public object Valor { get; set; }
    }

    public class ParametersSpETList : List<ParametersSpET> { }
}
