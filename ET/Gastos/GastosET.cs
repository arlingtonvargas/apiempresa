﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ET.Gastos
{
    public class GastosET
    {
        public int IdGasto { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public decimal valor { get; set; }
    }
}
