﻿using System;
using System.Collections.Generic;
using System.Text;
using DA.RegistroStock;
using ET.RegistroStock;

namespace BL.RegistroStock
{
    public class RegistroStockBL
    {

        RegistroStockDA clRegistroStock = new RegistroStockDA();
        public List<RegistroStockET> GetRegistrosStock()
        {
            try
            {
                return clRegistroStock.GetRegistrosStock();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public RegistroStockET GetRegistroStock(int SecReg)
        {
            try
            {
                return clRegistroStock.GetRegistroStock(SecReg);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public RegistroStockET InsertRegistroStock(RegistroStockET Reg)
        {
            try
            {
                return clRegistroStock.InsertRegistroStock(Reg);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
