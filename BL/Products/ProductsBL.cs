﻿using System;
using System.Collections.Generic;
using System.Text;
using DA.Producs;
using ET.Productos;

namespace BL.Products
{    
    public class ProductsBL
    {
        ProdcutsDA clProductos = new ProdcutsDA();
        public List<ProductsET> GetProducts()
        {
            try
            {
                return clProductos.GetProducts();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ProductsET GetProduct(int SecProd)
        {
            try
            {
                return clProductos.GetProduct(SecProd);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ProductsET InsertProduct(ProductsET Product)
        {
            try
            {
                return clProductos.InsertProduct(Product);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ProductsET UpdateProduct(ProductsET Product)
        {
            try
            {
                return clProductos.UpdateProduct(Product);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool DeleteProduct(int SecProd)
        {
            try
            {
                return clProductos.DeleteProduct(SecProd);
            }
            catch (Exception ex)
            {
                return false;
            }
        }       
    }
}
