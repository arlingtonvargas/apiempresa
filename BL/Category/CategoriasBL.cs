﻿using System;
using System.Collections.Generic;
using System.Text;
using DA.Category;
using ET.Category;

namespace BL.Category
{
    public class CategoriasBL
    {
        CategoriasDA clCategories = new CategoriasDA();
        public List<CategoriasET> GetCategories()
        {
            try
            {
                return clCategories.GetCategories();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public CategoriasET GetCategory(int SecProd)
        {
            try
            {
                return clCategories.GetCategory(SecProd);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public CategoriasET InsertCategory(CategoriasET Category)
        {
            try
            {
                return clCategories.InsertCategory(Category);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public CategoriasET UpdateCategory(CategoriasET Product)
        {
            try
            {
                return clCategories.UpdateCategory(Product);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool DeleteCategory(int SecProd)
        {
            try
            {
                return clCategories.DeleteCategory(SecProd);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
