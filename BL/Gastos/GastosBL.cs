﻿using System;
using System.Collections.Generic;
using System.Text;
using DA.Gastos;
using ET.Gastos;

namespace BL.Gastos
{
    public class GastosBL
    {
        GastosDA clGastos = new GastosDA();
        public List<GastosET> GetGastos()
        {
            try
            {
                return clGastos.GetGastos();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public GastosET GetGasto(int IdGasto)
        {
            try
            {
                return clGastos.GetGasto(IdGasto);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<GastosET> InsertGastos(List<GastosET> Gastos)
        {
            try
            {
                return clGastos.InsertGastos(Gastos);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
