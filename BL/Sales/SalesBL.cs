﻿using System;
using System.Collections.Generic;
using System.Text;
using ET.Sales;
using DA.Sales;

namespace BL.Sales
{
    public class SalesBL
    {
        SalesDA clSales = new SalesDA();
        public List<VentasET> GetSales()
        {
            try
            {
                return clSales.GetSales();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public VentasET GetSale(int SecSale)
        {
            try
            {
                return clSales.GetSale(SecSale);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public VentasET InsertSales(VentasET Sales)
        {
            try
            {
                return clSales.InsertSale(Sales);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
