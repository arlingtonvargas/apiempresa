﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Transactions;
using DA.RegistroStock;
using ET.Parameters;
using ET.RegistroStock;

namespace DA.RegistroStock
{
    public class RegistroStockDA
    {
        public List<RegistroStockET> GetRegistrosStock()
        {
            try
            {
                List<RegistroStockET> Ventas = Funtions.Functions.DataReaderMapToListConSP<RegistroStockET>("SP_RegistrosStock_G");
                Ventas.ForEach(x =>
                {
                    ParametersSpETList listaParam = new ParametersSpETList()
                {
                    new ParametersSpET() { NombreParametro = "@pIdRegistroStock", Valor = x.Sec.ToString() }
                };
                    x.RegStockDetalle = Funtions.Functions.DataReaderMapToListConSP<RegistroStockDetalleET>("SP_RegistroStockDetail_G", listaParam);
                });
                return Ventas;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public RegistroStockET GetRegistroStock(int SecReg)
        {
            try
            {
                ParametersSpETList listaParam = new ParametersSpETList()
                {
                    new ParametersSpET() { NombreParametro = "@pSec", Valor = SecReg.ToString() }
                };
                object Venta = Funtions.Functions.DataReaderMapToObjectConSP<RegistroStockET>("SP_RegistroStockById_G", listaParam);
                listaParam = new ParametersSpETList()
                {
                    new ParametersSpET() { NombreParametro = "@pIdRegistroStock", Valor = SecReg.ToString() }
                };
                object Vdetalle = Funtions.Functions.DataReaderMapToListConSP<RegistroStockDetalleET>("SP_RegistroStockDetail_G", listaParam);
                ((RegistroStockET)Venta).RegStockDetalle = (List<RegistroStockDetalleET>)Vdetalle;
                return (RegistroStockET)Venta;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public RegistroStockET InsertRegistroStock(RegistroStockET Reg)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(Conexion.Connection.getCadConnection()))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_RegistroStock_I", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.Add("@pSec", SqlDbType.Int).Direction = ParameterDirection.Output;
                        comando.Parameters.AddWithValue("@pFechaReg", Reg.FechaEnvio);
                        comando.Parameters.AddWithValue("@pFechaEnvio", Reg.FechaReg);
                        comando.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        bool Inserto = Convert.ToBoolean(comando.Parameters["@Inserto"].Value);
                        if (!Inserto) return null;
                        Reg.Sec = Convert.ToInt32(comando.Parameters["@pSec"].Value);
                        //Insert detail
                        foreach (RegistroStockDetalleET x in Reg.RegStockDetalle)
                        {
                            Inserto = false;
                            comando = new SqlCommand("SP_RegistroStockDetail_I", con);
                            comando.CommandType = CommandType.StoredProcedure;
                            comando.Parameters.Add("@pIdRegistroStockDetalle", SqlDbType.Int).Direction = ParameterDirection.Output;
                            comando.Parameters.AddWithValue("@pIdRegistroStock", Reg.Sec);
                            comando.Parameters.AddWithValue("@pIdProd", x.IdProd);
                            comando.Parameters.AddWithValue("@pCantidad", x.Cantidad);
                            comando.Parameters.AddWithValue("@pCantActual", x.CantActual);
                            comando.Parameters.AddWithValue("@pCantFinal", x.CantFinal);
                            comando.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                            comando.ExecuteNonQuery();
                            Inserto = Convert.ToBoolean(comando.Parameters["@Inserto"].Value);
                            if (!Inserto) return null;
                            x.IdRegistroStockDetalle = Convert.ToInt32(comando.Parameters["@pIdRegistroStockDetalle"].Value);
                        }
                    }
                    scope.Complete();
                }
                return Reg;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
