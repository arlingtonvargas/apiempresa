﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Transactions;
using ET.Parameters;
using ET.Productos;

namespace DA.Producs
{
    public class ProdcutsDA
    {
        public List<ProductsET> GetProducts()
        {
            try
            {
                return Funtions.Functions.DataReaderMapToListConSP<ProductsET>("SP_Products_G");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ProductsET GetProduct(int SecProd)
        {
            try
            {
                ParametersSpETList listaParam = new ParametersSpETList()
                {
                    new ParametersSpET() { NombreParametro = "@pSec", Valor=SecProd.ToString() }
                };
                object obj = Funtions.Functions.DataReaderMapToObjectConSP<ProductsET>("SP_ProductById_G", listaParam);
                return (ProductsET)obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ProductsET InsertProduct(ProductsET Product)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(Conexion.Connection.getCadConnection()))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_Product_I", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.Add("@pSec", SqlDbType.Int).Direction = ParameterDirection.Output;
                        comando.Parameters.AddWithValue("@pNombre", Product.Nombre);
                        comando.Parameters.AddWithValue("@pDescripcion", Product.Descripcion);
                        comando.Parameters.AddWithValue("@pCantidad", Product.Cantidad);
                        comando.Parameters.AddWithValue("@pPrecioCosto", Product.PrecioCosto);
                        comando.Parameters.AddWithValue("@pPrecioVenta", Product.PrecioVenta);
                        comando.Parameters.AddWithValue("@pFechaReg", Product.FechaReg);
                        comando.Parameters.AddWithValue("@pFechaMod", Product.FechaMod);
                        comando.Parameters.AddWithValue("@pIdCategoria", Product.IdCategoria);
                        comando.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        bool Inserto = Convert.ToBoolean(comando.Parameters["@Inserto"].Value);
                        if (!Inserto) return null;
                        Product.Sec = Convert.ToInt32(comando.Parameters["@pSec"].Value);

                    }
                    scope.Complete();
                }
                return Product;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ProductsET UpdateProduct(ProductsET Product)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(Conexion.Connection.getCadConnection()))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_Product_U", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@pSec", Product.Sec);
                        comando.Parameters.AddWithValue("@pNombre", Product.Nombre);
                        comando.Parameters.AddWithValue("@pDescripcion", Product.Descripcion);
                        comando.Parameters.AddWithValue("@pPrecioCosto", Product.PrecioCosto);
                        comando.Parameters.AddWithValue("@pPrecioVenta", Product.PrecioVenta);
                        comando.Parameters.AddWithValue("@pFechaMod", Product.FechaMod);
                        comando.Parameters.AddWithValue("@pIdCategoria", Product.IdCategoria);
                        comando.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        bool Inserto = Convert.ToBoolean(comando.Parameters["@Inserto"].Value);
                        if (!Inserto) return null;
                    }
                    scope.Complete();
                }
                return Product;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool DeleteProduct(int SecProd)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(Conexion.Connection.getCadConnection()))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_Product_D", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@pSec", SecProd.ToString());
                        comando.Parameters.Add("@EjecutoOK", SqlDbType.Bit).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        bool EjecutoOK = Convert.ToBoolean(comando.Parameters["@EjecutoOK"].Value);
                        if (!EjecutoOK) return false;
                    }
                    scope.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
