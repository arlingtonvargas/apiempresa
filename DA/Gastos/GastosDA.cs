﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Transactions;
using ET.Gastos;
using ET.Parameters;

namespace DA.Gastos
{
    public class GastosDA
    {
        public List<GastosET> GetGastos()
        {
            try
            {
                return Funtions.Functions.DataReaderMapToListConSP<GastosET>("SP_Gastos_G");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public GastosET GetGasto(int IdGasto)
        {
            try
            {
                ParametersSpETList listaParam = new ParametersSpETList()
                {
                    new ParametersSpET() { NombreParametro = "@pIdGasto", Valor = IdGasto.ToString() }
                };
                object obj = Funtions.Functions.DataReaderMapToObjectConSP<GastosET>("SP_GastoById_G", listaParam);
                return (GastosET)obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<GastosET> InsertGastos(List<GastosET> Gastos)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(Conexion.Connection.getCadConnection()))
                    {
                        con.Open();
                        foreach (GastosET x in Gastos)
                        {
                            SqlCommand comando = new SqlCommand("SP_Gasto_I", con);
                            comando.CommandType = CommandType.StoredProcedure;
                            comando.Parameters.Add("@pIdGasto", SqlDbType.Int).Direction = ParameterDirection.Output;
                            comando.Parameters.AddWithValue("@pDescripcion", x.Descripcion);
                            comando.Parameters.AddWithValue("@pFecha", x.Fecha);
                            comando.Parameters.AddWithValue("@pvalor", x.valor);
                            comando.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                            comando.ExecuteNonQuery();
                            bool Inserto = Convert.ToBoolean(comando.Parameters["@Inserto"].Value);
                            if (!Inserto) return null;
                            x.IdGasto = Convert.ToInt32(comando.Parameters["@pIdGasto"].Value);
                        }                       
                    }
                    scope.Complete();
                }
                return Gastos;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
