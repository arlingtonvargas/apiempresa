﻿using System;
using System.Collections.Generic;
using System.Text;
using ET.Parameters;
using ET.Category;
using System.Transactions;
using System.Data.SqlClient;
using System.Data;

namespace DA.Category
{
    public class CategoriasDA
    {
        public List<CategoriasET> GetCategories()
        {
            try
            {
                return Funtions.Functions.DataReaderMapToListConSP<CategoriasET>("SP_Categories_G");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public CategoriasET GetCategory(int SecProd)
        {
            try
            {
                ParametersSpETList listaParam = new ParametersSpETList()
                {
                    new ParametersSpET() { NombreParametro = "@pIdCat", Valor=SecProd.ToString() }
                };
                object obj = Funtions.Functions.DataReaderMapToObjectConSP<CategoriasET>("SP_CategoryById_G", listaParam);
                return (CategoriasET)obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public CategoriasET InsertCategory(CategoriasET Category)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(Conexion.Connection.getCadConnection()))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_Category_I", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.Add("@pIdCat", SqlDbType.Int).Direction = ParameterDirection.Output;
                        comando.Parameters.AddWithValue("@pNomCat", Category.NomCat);
                        comando.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        bool Inserto = Convert.ToBoolean(comando.Parameters["@Inserto"].Value);
                        if (!Inserto) return null;
                        Category.IdCat = Convert.ToInt32(comando.Parameters["@pIdCat"].Value);

                    }
                    scope.Complete();
                }
                return Category;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public CategoriasET UpdateCategory(CategoriasET Category)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(Conexion.Connection.getCadConnection()))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_Category_U", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@pIdCat", Category.IdCat);
                        comando.Parameters.AddWithValue("@pNomCat", Category.NomCat);
                        comando.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        bool Inserto = Convert.ToBoolean(comando.Parameters["@Inserto"].Value);
                        if (!Inserto) return null;
                    }
                    scope.Complete();
                }
                return Category;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool DeleteCategory(int IdCat)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(Conexion.Connection.getCadConnection()))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_Category_D", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@pIdCat", IdCat.ToString());
                        comando.Parameters.Add("@EjecutoOK", SqlDbType.Bit).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        bool EjecutoOK = Convert.ToBoolean(comando.Parameters["@EjecutoOK"].Value);
                        if (!EjecutoOK) return false;
                    }
                    scope.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
