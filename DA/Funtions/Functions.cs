﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using ET.Parameters;

namespace DA.Funtions
{
    public class Functions
    {
        public static List<T> DataReaderMapToListConSP<T>(string SPname, List<ParametersSpET> sqlParameters = null )
        {
            List<T> list = new List<T>();
            try
            {
                using (SqlConnection conn = new SqlConnection(Conexion.Connection.getCadConnection()))
                {
                    SqlCommand cmd = new SqlCommand(SPname, conn);
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    if(sqlParameters!=null)sqlParameters.ForEach(x => { cmd.Parameters.AddWithValue(x.NombreParametro, x.Valor); });
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        T obj = default(T);
                        while (dr.Read())
                        {
                            obj = Activator.CreateInstance<T>();
                            foreach (PropertyInfo prop in obj.GetType().GetProperties())
                            {
                                if (!object.Equals(dr[prop.Name], DBNull.Value))
                                {
                                    prop.SetValue(obj, dr[prop.Name], null);
                                }
                            }
                            list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ClRegistraLogs.RegistrarError(new ET.SGLogs()
                //{
                //    FechaReg = DateTime.Now,
                //    MensajeError = ex.Message,
                //    NomArchivo = "Funciones",
                //    NomError = "Exception",
                //    NomFunction = "DataReaderMapToList",
                //    NomModulo = "General",
                //    NumLinea = 325,
                //    Sec = 0
                //});
                return null;
            }

            return list;
        }

        public static object DataReaderMapToObjectConSP<T>(string SPname, List<ParametersSpET> sqlParameters = null)
        {
            T obj = default(T);
            try
            {
                using (SqlConnection conn = new SqlConnection(Conexion.Connection.getCadConnection()))
                {
                    SqlCommand cmd = new SqlCommand(SPname, conn);
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (sqlParameters != null) sqlParameters.ForEach(x => { cmd.Parameters.AddWithValue(x.NombreParametro, x.Valor); });
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            obj = Activator.CreateInstance<T>();
                            foreach (PropertyInfo prop in obj.GetType().GetProperties())
                            {
                                if (!object.Equals(dr[prop.Name], DBNull.Value))
                                {
                                    prop.SetValue(obj, dr[prop.Name], null);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ClRegistraLogs.RegistrarError(new ET.SGLogs()
                //{
                //    FechaReg = DateTime.Now,
                //    MensajeError = ex.Message,
                //    NomArchivo = "Funciones",
                //    NomError = "Exception",
                //    NomFunction = "DataReaderMapToList",
                //    NomModulo = "General",
                //    NumLinea = 325,
                //    Sec = 0
                //});
                return null;
            }
            return obj;
        }


        public static List<T> DataReaderMapToList<T>(string sql)
        {
            List<T> list = new List<T>();
            try
            {
                using (SqlConnection conn = new SqlConnection(Conexion.Connection.getCadConnection()))
                {
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    conn.Open();
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        T obj = default(T);
                        while (dr.Read())
                        {
                            obj = Activator.CreateInstance<T>();
                            foreach (PropertyInfo prop in obj.GetType().GetProperties())
                            {
                                if (!object.Equals(dr[prop.Name], DBNull.Value))
                                {
                                    prop.SetValue(obj, dr[prop.Name], null);
                                }
                            }
                            list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ClRegistraLogs.RegistrarError(new ET.SGLogs()
                //{
                //    FechaReg = DateTime.Now,
                //    MensajeError = ex.Message,
                //    NomArchivo = "Funciones",
                //    NomError = "Exception",
                //    NomFunction = "DataReaderMapToList",
                //    NomModulo = "General",
                //    NumLinea = 325,
                //    Sec = 0
                //});
                return null;
            }

            return list;
        }

        public static DataTable Consultar(string sql, SqlConnection con)
        {
            try
            {
                SqlConnection conec = con;
                SqlCommand comando = new SqlCommand(sql, conec);
                SqlDataAdapter datos = new SqlDataAdapter(comando);
                DataTable tabla = new DataTable();
                try
                {
                    datos.Fill(tabla);
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                }
                return tabla;
            }
            catch (Exception)
            { return new DataTable(); }

        }


        public static string EjecutaComando(string sql, SqlConnection con)
        {
            sql = "set dateformat dmy; " + sql;
            SqlConnection conexion = con;
            SqlCommand comando = new SqlCommand(sql);
            comando.Connection = conexion;
            string val = "no";
            try
            {
                int fill_afec = comando.ExecuteNonQuery();
                if (fill_afec > 0)
                {
                    val = "si";
                }
            }
            catch (System.Exception ex)
            {
                //ClRegistraLogs.RegistrarError(new ET.SGLogs()
                //{
                //    FechaReg = DateTime.Now,
                //    MensajeError = ex.Message,
                //    NomArchivo = "Funciones",
                //    NomError = "Exception",
                //    NomFunction = "EjecutaComando",
                //    NomModulo = "General",
                //    NumLinea = 325,
                //    Sec = 0
                //});
                return ex.Message.ToString();
            }
            finally
            {
            }
            return val;
        }

        public static int EjecutaComandoReturId(string sql, SqlConnection con)
        {
            sql = "set dateformat dmy; " + sql;
            SqlConnection conexion = con;
            SqlCommand comando = new SqlCommand(sql);
            comando.Connection = conexion;
            try
            {
                var fill_afec = comando.ExecuteScalar();
                return Convert.ToInt32(fill_afec);
            }
            catch (System.Exception a)
            {
                return -1;
            }
            finally
            {
            }
        }

        public static int TraeSiguienteSecuencial(string tabla, string campo, SqlConnection con)
        {
            try
            {
                string sql = string.Format("SELECT MAX(ISNULL({0},0))+1 FROM {1}", campo, tabla);
                DataTable dt = Consultar(sql, con);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0] == DBNull.Value)
                    {
                        return 1;
                    }
                    else
                    {
                        return (int)(dt.Rows[0][0]);
                    }
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static bool ExisteDato(string tabla, string campo, string valor, SqlConnection con)
        {
            try
            {
                string sql = string.Format("SELECT * FROM {0} WHERE {1} = '{2}'", tabla, campo, valor);
                DataTable dt = Consultar(sql, con);
                if (dt.Rows.Count > 0) { return true; } else { return false; }
            }
            catch (Exception ex)
            {
                return true;
            }
        }

    }
}
