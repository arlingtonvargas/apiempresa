﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Transactions;
using ET.Parameters;
using ET.Sales;

namespace DA.Sales
{
    public class SalesDA
    {
        public List<VentasET> GetSales()
        {
            try
            {
                List<VentasET> Ventas = Funtions.Functions.DataReaderMapToListConSP<VentasET>("SP_Sales_G");
                Ventas.ForEach(x => 
                {
                    ParametersSpETList listaParam = new ParametersSpETList()
                {
                    new ParametersSpET() { NombreParametro = "@pIdVenta", Valor = x.Sec.ToString() }
                };
                    x.VentasDetalle = Funtions.Functions.DataReaderMapToListConSP<VentaDetalleET>("SP_SalesDetail_G", listaParam);
                });
                return Ventas;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public VentasET GetSale(int SecSale)
        {
            try
            {
                ParametersSpETList listaParam = new ParametersSpETList()
                {
                    new ParametersSpET() { NombreParametro = "@pSec", Valor = SecSale.ToString() }
                };
                object Venta = Funtions.Functions.DataReaderMapToObjectConSP<VentasET>("SP_Sale_G", listaParam);
                listaParam = new ParametersSpETList()
                {
                    new ParametersSpET() { NombreParametro = "@pIdVenta", Valor = SecSale.ToString() }
                };
                object Vdetalle = Funtions.Functions.DataReaderMapToListConSP<VentaDetalleET>("SP_SalesDetail_G", listaParam);                
                ((VentasET)Venta).VentasDetalle = (List<VentaDetalleET>)Vdetalle;
                return (VentasET)Venta;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public VentasET InsertSale(VentasET Sale)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(Conexion.Connection.getCadConnection()))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_Sale_I", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.Add("@pSec", SqlDbType.Int).Direction = ParameterDirection.Output;
                        comando.Parameters.AddWithValue("@pFechaVenta", Sale.FechaVenta);
                        comando.Parameters.AddWithValue("@pFechaRegistro", Sale.FechaRegistro);
                        comando.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        bool Inserto = Convert.ToBoolean(comando.Parameters["@Inserto"].Value);
                        if (!Inserto) return null;
                        Sale.Sec = Convert.ToInt32(comando.Parameters["@pSec"].Value);
                        //Insert detail
                        foreach (VentaDetalleET x in Sale.VentasDetalle)
                        {
                            Inserto = false;
                            comando = new SqlCommand("SP_SaleDetail_I", con);
                            comando.CommandType = CommandType.StoredProcedure;
                            comando.Parameters.Add("@pIdVentaDetalle", SqlDbType.Int).Direction = ParameterDirection.Output;
                            comando.Parameters.AddWithValue("@pIdVenta", Sale.Sec);
                            comando.Parameters.AddWithValue("@pIdProd", x.IdProd);
                            comando.Parameters.AddWithValue("@pCantidad", x.Cantidad);
                            comando.Parameters.AddWithValue("@pValUni", x.ValUni);
                            comando.Parameters.AddWithValue("@pValTo", x.ValTo);
                            comando.Parameters.Add("@Inserto", SqlDbType.Bit).Direction = ParameterDirection.Output;
                            comando.ExecuteNonQuery();
                            Inserto = Convert.ToBoolean(comando.Parameters["@Inserto"].Value);
                            if (!Inserto) return null;
                            x.IdVentaDetalle = Convert.ToInt32(comando.Parameters["@pIdVentaDetalle"].Value);
                        }                        
                    }
                    scope.Complete();
                }
                return Sale;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
